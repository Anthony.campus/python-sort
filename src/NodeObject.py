class Node:

    def __init__(self, value):
        self.value = value

        self.acctive = True
        self.right = None
        self.left = None
        self.parent = None

    def setParent(self, parent):
        self.parent = parent
        self.switchValueParent()

    def addNode(self, node):
        if self.acctive:
            if self.left == None:
                self.left = node
                self.left.setParent(self)
            else:
                self.left.addNode(node)
        else:
            if self.right == None:
                self.right = node
                self.right.setParent(self)
            else:
                self.right.addNode(node)
        self.acctive = not self.acctive

    def switchValueParent(self):
        if self.parent != None:
            if self.parent.value < self.value:
                v = self.value
                self.value = self.parent.value
                self.parent.value = v
                self.parent.switchValueParent()

    def switchValueSon(self):
        if self.left.value > self.right.value: #L'enfant de gauche à la plus grosse Alors je fais remonter la droite
            if self.value > self.right.value:
                v = self.value
                self.value = self.right.value
                self.right.value = v
                self.right.switchValueSon()
        else:
            if self.value > self.left.value:
                v = self.value
                self.value = self.left.value
                self.left.value = v
                self.left.switchValueSon()

    def restoreValue(self):
        if self.parent == None: #1er de fill
            lastSon = self.getLastSon()
            if lastSon == None:
                return None
            
            lastSon.parent = None

            v = self.value
            self.value = lastSon.value
            
            return v
            
    def getLastSon(self):
        if self.left == None and self.right == None:
            return None

        if self.acctive: # Obliger d'avoir un droite si il y a un gauche
            self.acctive = not self.acctive
            return self.right.getLastSon()
        else:
            if self.left != None:
                self.acctive = not self.acctive
                return self.left.getLastSon()
            else:
                if self.parent.left == self:
                    self.parent.left = None
                else:
                    self.parent.right = None
                return self

